# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 16:34:46 2020

@author: 54797
"""

import urllib.request
from bs4 import BeautifulSoup
import ssl
import xlwt

ssl._create_default_https_context = ssl._create_unverified_context
list_href = []
for num in range(0,250,25):
    url = "https://movie.douban.com/top250?start=" + str(num)
    # User-Agent头
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(req)
    # 获取每页的HTML源码字符串
    html = response.read().decode('utf-8')
    # 解析html 为 Beautiful Soup 对象
    soup = BeautifulSoup(html, "lxml")
    
    for item in soup.find_all("div", "hd"):
        href = item.find('a').get('href')
        list_href.append(href)
        
#print(list_href)
book = xlwt.Workbook(encoding='utf-8')
sheet = book.add_sheet('sheet1')
head = ['排名','链接']
for h in range(len(head)):
    sheet.write(0,h,head[h])
for i in range(0,250):
    sheet.write(i+1,0,i+1)
    sheet.write(i+1,1,list_href[i])
book.save(r'C:\Users\54797\Desktop\worm\demo2.xls')
print("done")