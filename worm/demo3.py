# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:38:30 2020

@author: 54797
"""
import requests
import urllib.request
from bs4 import BeautifulSoup
import ssl
import re
#import xlwt

index = 1

ssl._create_default_https_context = ssl._create_unverified_context
list_pic_href = []
for num in range(0,250,25):
    url = "https://movie.douban.com/top250?start=" + str(num)
    # User-Agent头
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(req)
    # 获取每页的HTML源码字符串
    html = response.read().decode('utf-8')
    # 解析html 为 Beautiful Soup 对象
    soup = BeautifulSoup(html, "lxml")
    
    for item in soup.find_all("div", "pic"):
        href = item.find('img').get('src')
        list_pic_href.append(href)
    
    pictures_part = re.findall('<div class="pic">(.*?)</div>', html, re.S)
    for picture in pictures_part:
        img = re.findall('src="(.*?)" class', picture, re.S)
        pic = requests.get(img[0], headers=headers)
        fp = open('D:\wamp\www\worm\imgs\\' + str(index) + '.jpg', 'wb')  ####这里选用wb以二进制形式写入文件
        fp.write(pic.content)
        fp.close()
        print('picture' + str(index) + ' has been dawnload')
        index += 1
        
#print(list_pic_href)
#book = xlwt.Workbook(encoding='utf-8')
#sheet = book.add_sheet('sheet1')
#head = ['排名','图片链接']
#for h in range(len(head)):
#    sheet.write(0,h,head[h])
#for i in range(0,250):
#    sheet.write(i+1,0,i+1)
#    sheet.write(i+1,1,list_pic_href[i])
#book.save(r'C:\Users\54797\Desktop\worm\demo3.xls')
print("done")