# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 09:17:23 2020

@author: 54797
"""
import urllib.request
from bs4 import BeautifulSoup
import ssl
import xlwt
import re

# 取消代理验证
ssl._create_default_https_context = ssl._create_unverified_context
list_name = []
list_director_and_actor = []
list_point = []
list_quote = []
list_star_num = []
for num in range(0,250,25):
    url = "https://movie.douban.com/top250?start=" + str(num)
    # User-Agent头
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(req)
    # 获取每页的HTML源码字符串
    html = response.read().decode('utf-8')
    # 解析html 为 Beautiful Soup 对象
    soup = BeautifulSoup(html, "lxml")
    
    #for text in soup.select('em'):
    #    list_rank.append(text.get_text())

    for item in soup.find_all("div", "info"):
        name = item.div.a.span.string
        list_name.append(name)
#        director_and_actor = item.find('div','bd').p.get_text().replace(' ','')
#        list_director_and_actor.append(director_and_actor)
        point = item.find('div','star').find('span','rating_num').string
        list_point.append(point)
        quote = item.find('div','bd').find('span','inq')
        if quote != None:
            list_quote.append(quote.get_text())
        else:
            list_quote.append('无')
        star_num = item.find('div','star').find_all('span')[3].string
        list_star_num.append(star_num)
#print(list_star_num)
book = xlwt.Workbook(encoding='utf-8')
sheet = book.add_sheet('sheet1')
head = ['排名','名称','评分','评分人数','短评']
for h in range(len(head)):
    sheet.write(0,h,head[h])
for i in range(0,250):
    sheet.write(i+1,0,i+1)
    sheet.write(i+1,1,list_name[i])
    sheet.write(i+1,2,list_point[i])
    sheet.write(i+1,3,list_star_num[i])
    sheet.write(i+1,4,list_quote[i])
book.save(r'C:\Users\54797\Desktop\worm\demo.xls')
print("done")