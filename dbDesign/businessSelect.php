<?php
header("Content-Type:text/html;charset=utf-8");
$conn=mysql_connect("127.0.0.1:3320","root","xsh19990904");
mysql_select_db("xushenghao02mis",$conn);
if (!empty($_POST['select1'])){
	$sql = "SELECT xush_students.Sno,xush_students.Sname,xush_courses.CoName,xush_courses.CoTerm,xush_teachers.Tname,xush_scores.Sscore FROM xush_students INNER JOIN
		xush_scores ON xush_students.Sno=xush_scores.Sno INNER JOIN xush_courses ON xush_courses.CoNo=xush_scores.CoNo
		INNER JOIN xush_teachers ON xush_teachers.Tno=xush_scores.Tno ORDER BY xush_students.Sno ASC,xush_courses.CoTerm DESC;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>学生成绩按学年统计</title>
		<style>
			div {
				width: 700px;
				height: 700px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"60%\" height=\"60%\">
			<tr>
				<th>学号</th>
				<th>学生姓名</th>
				<th>课程名</th>
				<th>开设学年</th>
				<th>教师名</th>
				<th>成绩</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Sno'];
					echo "</td>
					<td>";
					echo $row['Sname'];
					echo "</td>
					<td>";
					echo $row['CoName'];
					echo "</td>
					<td>";
					echo $row['CoTerm'];
					echo "</td>
					<td>";
					echo $row['Tname'];
					echo "</td>
				<td>";
					echo $row['Sscore'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>	
		</div>
		</body>
		</html>";
}

if (!empty($_POST['select2'])){
	$sql = "SELECT xush_students.Sno,xush_students.Sname,xush_courses.CoName,xush_teachers.Tname,AVG(xush_scores.Sscore) AverageScore FROM xush_students INNER JOIN
		xush_scores ON xush_students.Sno=xush_scores.Sno INNER JOIN xush_courses ON xush_courses.CoNo=xush_scores.CoNo
		INNER JOIN xush_teachers ON xush_teachers.Tno=xush_scores.Tno GROUP BY xush_scores.Sno
		ORDER BY AverageScore DESC;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>学生成绩名次</title>
		<style>
			div {
				width: 500px;
				height: 500px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"50%\" height=\"50%\">
			<tr>
				<th>学号</th>
				<th>学生姓名</th>
				<th>平均成绩</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Sno'];
					echo "</td>
					<td>";
					echo $row['Sname'];
					echo "</td>
				<td>";
					echo $row['AverageScore'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>
		</div>	
		</body>
		</html>";
}

if (!empty($_POST['select3'])){
	$sql = "SELECT xush_courses.CoNo,xush_courses.CoName,AVG(xush_scores.Sscore) AverageScore FROM xush_courses
		INNER JOIN xush_scores ON xush_courses.CoNo=xush_scores.CoNo GROUP BY xush_scores.CoNo;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>每门课程平均成绩统计</title>
		<style>
			div {
				width: 500px;
				height: 500px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"50%\" height=\"50%\">
			<tr>
				<th>课程号</th>
				<th>课程名</th>
				<th>平均成绩</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['CoNo'];
					echo "</td>
					<td>";
					echo $row['CoName'];
					echo "</td>
				<td>";
					echo $row['AverageScore'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>	
		</div>
		</body>
		</html>";
}

if (!empty($_POST['select4'])){
	$sql = "SELECT xush_students.Sno,xush_students.Sname,xush_courses.CoName,xush_courses.CoCredit FROM xush_students INNER JOIN xush_scores ON xush_students.Sno=xush_scores.Sno INNER JOIN xush_courses ON xush_courses.CoNo=xush_scores.CoNo ORDER BY xush_students.Sno;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>学生所学课程及学分统计</title>
		<style>
			div {
				width: 600px;
				height: 600px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"50%\" height=\"50%\">
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>课程名</th>
				<th>学分</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Sno'];
					echo "</td>
					<td>";
					echo $row['Sname'];
					echo "</td>
					<td>";
					echo $row['CoName'];
					echo "</td>
				<td>";
					echo $row['CoCredit'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>	
		</div>
		</body>
		</html>";
}

if (!empty($_POST['select5'])){
	$sql = "SELECT xush_students.Sno,xush_students.Sname,xush_courses.CoName,xush_teachers.Tname,xush_scores.Sscore
		FROM xush_students 
		INNER JOIN xush_scores
		ON xush_students.Sno=xush_scores.Sno
		INNER JOIN xush_courses
		ON xush_courses.CoNo=xush_scores.CoNo
		INNER JOIN xush_teachers
		ON xush_scores.Tno=xush_teachers.Tno;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>学生成绩查询</title>
		<style>
			div {
				width: 600px;
				height: 600px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"60%\" height=\"60%\">
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>课程名</th>
				<th>教师名</th>
				<th>成绩</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Sno'];
					echo "</td>
					<td>";
					echo $row['Sname'];
					echo "</td>
					<td>";
					echo $row['CoName'];
					echo "</td>
					<td>";
					echo $row['Tname'];
					echo "</td>
				<td>";
					echo $row['Sscore'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>
		</div>	
		</body>
		</html>";
}

if (!empty($_POST['select6'])){
	$sql = "SELECT xush_teachers.Tname,xush_courses.CoName FROM xush_teachers INNER JOIN xush_teachercourse ON xush_teachers.Tno=xush_teachercourse.Tno INNER JOIN xush_courses ON xush_courses.CoNo=xush_teachercourse.CoNo
		ORDER BY xush_teachercourse.Tno;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>教师任课查询</title>
		<style>
			div {
				width: 500px;
				height: 500px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"50%\" height=\"50%\">
			<tr>
				<th>教师名</th>
				<th>课程名</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Tname'];
					echo "</td>
			
				<td>";
					echo $row['CoName'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>
		</div>	
		</body>
		</html>";
}

if (!empty($_POST['select7'])){
	$sql = "SELECT xush_classes.Cno,xush_courses.CoName FROM xush_classes INNER JOIN xush_classcourse ON xush_classes.Cno=xush_classcourse.Cno INNER JOIN xush_courses ON xush_courses.CoNo=xush_classcourse.CoNo ORDER BY xush_classes.Cno;";
	$res = mysql_query($sql,$conn);
	echo"
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset=\"utf-8\">
		<title>班级课程开设查询</title>
		<style>
			div {
				width: 500px;
				height: 500px;
			}

			.main{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align:center;
			}
		</style>
	</head>
	<body>
		<div class=\"main\">
		<table align=\"center\" border=\"1\" width=\"50%\" height=\"50%\">
			<tr>
				<th>班级号</th>
				<th>课程名</th>
			</tr>";
	while($row=mysql_fetch_assoc($res)){
	echo "
			<tr>
				<td>";
					echo $row['Cno'];
					echo "</td>
			
				<td>";
					echo $row['CoName'];
					echo "</td>
			</tr>";
			}
	echo "</table>
		<a href=\"businessSelect.html\">返回业务查询页面</a>	
		</div>
		</body>
		</html>";
}
?>